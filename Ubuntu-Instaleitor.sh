#!/usr/bin/env bash
# -*- ENCODING: UTF-8 -*-
# Ubuntu-Instaleitor
# Author: Carlos Gómez
# Twitter: @xxRasAlGhulxx

clear

# check for root user
if [[ $EUID -ne 0 ]]; then
echo "Ubuntu-Instaleitor: This script can only be executed by root."
echo "Please try with: sudo ./Ubuntu-Instaleitor.sh" 1>&2
exit 1
fi

# Create a secure tmp directory
tmp=${TMPDIR-/tmp}
	tmp=$tmp/Ubuntu-Instaleitor.$RANDOM.$RANDOM.$RANDOM.$$ # Use a random name so it's secure
	(umask 077 && mkdir "$tmp") || { # Another security precaution
		echo "Could not create temporary directory! Exiting." 1>&2
		exit 1
	}

# Checks if terminal supports colors
if [ $(tput colors) ]; then
  red="\e[31m"
  green="\e[32m"
	blue="\e[34m"
	yellow="\e[33m"
	cyan="\e[36m"
	purple="\e[0;35m"
  endcolor="\e[39m"
fi

clear

# Welcome screen
echo --------------------------------------------------------------------------------
echo
echo  "\n \t Ubuntu-Instaleitor"
echo
echo  "\n \t v.01 beta"
echo
echo --------------------------------------------------------------------------------
echo
echo -e "\t $red We are not responsible for any damages that may possibly $endcolor";
echo -e "\t $red occur while using Ubuntu-Instaleitor on your computer $endcolor";
echo -e "$yellow PLEASE CLOSE ALL APPS!! SISTEM WILL BE REBOOT WHEN FINISH THE INSTALATION $endcolor";
echo "   ";

echo -e "$green This may take a while, please wait until Ubuntu-Instaleitor has finished $endcolor";
echo -e "$green Cancel: Ctrl+c $endcolor";
echo "   ";
sleep 5

# Removing default configurations
echo -e "$blue Removing lens from dash $endcolor"
(
	gsettings set com.canonical.Unity.Lenses disabled-scopes "['more_suggestions-amazon.scope', 'more_suggestions-u1ms.scope', 'more_suggestions-populartracks.scope', 'music-musicstore.scope', 'more_suggestions-ebay.scope', 'more_suggestions-ubuntushop.scope', 'more_suggestions-skimlinks.scope']"
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$yellow Do you want turn off system crash dialogs (apport)? $endcolor"
echo "y"/"n"
read apport
echo
if [[ $apport == y ]]; then
	echo -e "$blue Turning off dialogs $endcolor"
	(
		sed -i "s/enabled=1/enabled=0/g" /etc/default/apport
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

# Add all the repositories
echo -e "$blue Adding Repositories $endcolor"
(
	add-apt-repository ppa:tualatrix/ppa -y
	add-apt-repository ppa:nilarimogard/webupd8 -y
	add-apt-repository ppa:fossfreedom/indicator-sysmonitor -y
	apt-add-repository ppa:rael-gc/scudcloud -y
	add-apt-repository ppa:gwendal-lebihan-dev/hexchat-stable -y
	add-apt-repository ppa:webupd8team/atom -y
	apt-add-repository ppa:webupd8team/brackets -y
	apt-add-repository ppa:webupd8team/sublime-text-3 -y
	add-apt-repository ppa:synapse-core/ppa -y
	add-apt-repository ppa:openjdk-r/ppa -y
	add-apt-repository ppa:linrunner/tlp -y
	add-apt-repository ppa:yannubuntu/boot-repair -y
	apt-add-repository ppa:numix/ppa -y
	add-apt-repository ppa:nitrux/nitrux-artwork -y
	apt-add-repository ppa:snwh/pulp -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Updating System $endcolor"
(
	apt-get update
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

# Start install
echo -e "$blue Installing ubuntu-tweak $endcolor"
(
	apt-get install ubuntu-tweak -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing unity-tweak-tool $endcolor"
(
	apt-get install unity-tweak-tool -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing dconf-tools $endcolor"
(
	apt-get install dconf-tools -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing indicator-netspeed $endcolor"
(
 apt-get install indicator-netspeed -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing indicator-sysmonitor $endcolor"
(
	apt-get install indicator-sysmonitor -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing redshift $endcolor"
(
	apt-get install redshift redshift-gtk -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing speedtest-cli $endcolor"
(
	apt-get install python-pip -y
	pip install speedtest-cli
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing VLC $endcolor"
(
	apt-get install vlc -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing qBittorrent $endcolor"
(
	apt-get install qbittorrent -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing recordmydesktop $endcolor"
(
	apt-get install recordmydesktop gtk-recordmydesktop -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing gimp $endcolor"
(
	apt-get install gimp -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Inkscape $endcolor"
(
	apt-get install inkscape -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing chromium-browser $endcolor"
(
	apt-get install chromium-browser -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing pepperflashplugin-nonfree $endcolor"
(
	apt-get install pepperflashplugin-nonfree -y
	update-pepperflashplugin-nonfree --install
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing compiz-plugins $endcolor"
(
	apt-get install compizconfig-settings-manager -y
	apt-get install compiz-plugins-extra -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Dia $endcolor"
(
	apt-get install dia -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing shutter $endcolor"
(
	apt-get install shutter -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing wireshark$endcolor"
(
	apt-get install wireshark -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing nautilus-open-terminal $endcolor"
(
	apt-get install nautilus-open-terminal -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Hexchat $endcolor"
(
	apt-get install hexchat -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Git $endcolor"
(
	apt-get install git -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Gparted $endcolor"
(
	apt-get install gparted -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Clementine $endcolor"
(
	apt-get install clementine -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing lm-sensors $endcolor"
(
	apt-get install lm-sensors -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing hddtemp $endcolor"
(
	apt-get install hddtemp -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing prelink (requires manual configuration)$endcolor"
(
	apt-get install prelink -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Htop $endcolor"
(
	apt-get install htop -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Preload $endcolor"
(
	apt-get install preload -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Terminator $endcolor"
(
	apt-get install terminator -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing synapse $endcolor"
(
	apt-get install synapse -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing cheese $endcolor"
(
	apt-get install cheese -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing gsmartcontrol $endcolor"
(
	apt-get install gsmartcontrol -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing TLP $endcolor"
(
	apt-get install tlp tlp-rdw -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing boot-repair $endcolor"
(
	apt-get install boot-repair -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Curl $endcolor"
(
	apt-get install curl -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing Beatiful Themes ;) $endcolor"
(
	apt-get install numix-icon-theme numix-icon-theme-circle -y
	apt-get install numix-gtk-theme -y
	apt-get install nitrux-icon-theme -y
	apt-get install paper-gtk-theme -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Setting up DVD's $endcolor"
(
apt-get install libdvdread4 -y
/usr/share/doc/libdvdread4/install-css.sh
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing build-essential $endcolor"
(
	apt-get install build-essential -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$blue Installing filezilla $endcolor"
(
	apt-get install filezilla -y
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo
echo ".+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+";
echo "From here you will need pay attention for install the software";
echo ".+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+";
echo
echo -e "$yellow Enter (y) or (n) when Ubuntu-Instaleitor ask (case sensitive) $endcolor"
echo

sleep 5s

echo -e "$blue Setting up ubuntu-restricted-extras $endcolor"
(
	apt-get install ubuntu-restricted-extras -y
) # &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo -e "$yellow Do you want to install K3B on your system? $endcolor"
echo "y"/"n"
read k3b
echo
if [[ $k3b == y ]]; then
	echo -e "$blue Installing K3B $endcolor $red MAY TAKE SEVERAL TIME $endcolor"
	(
		apt-get install k3b kde-l10n-es k3b-i18n k3b-extrathemes -y
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install Scudcloud (Slack client) on your system? $endcolor"
echo "y"/"n"
read scudcloud
echo
if [[ $scudcloud == y ]]; then
	echo -e "$blue Installing scudcloud $endcolor"
	(
		apt-get install scudcloud -y
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install Sublime Text 3 on your system? $endcolor"
echo "y"/"n"
read sublime
echo
if [[ $sublime == y ]]; then
	echo -e "$blue Sublime Text 3 $endcolor"
	(
		apt-get install sublime-text-installer
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install Atom on your system? $endcolor"
echo "y"/"n"
read atom
echo
if [[ $atom == y ]]; then
	echo -e "$blue Installing Atom $endcolor"
	(
		apt-get install atom
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install Brackets on your system? $endcolor"
echo "y"/"n"
read brackets
echo
if [[ $brackets == y ]]; then
	echo -e "$blue Installing brackets $endcolor"
	(
		apt-get install brackets
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install RVM for Ruby on your system? $endcolor"
echo "y"/"n"
read rvm
echo
if [[ $brackets == y ]]; then
	echo -e "$blue Installing RVM $endcolor"
	(
		gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
		\curl -sSL https://get.rvm.io | bash -s stable
		# source ~/.rvm/scripts/rvm
		# rvm requirements
		# rvm install ruby
		# rvm use ruby --default
		# rvm rubygems current
		# gem install rails
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install Spotify on your system? $endcolor"
echo "y"/"n"
read spotify
echo
if [[ $spotify == y ]]; then
	echo -e "$blue Installing Spotify $endcolor"
	(
		apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D2C19886
		echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
		apt-get update
		apt-get install spotify-client -y
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install Dropbox on your system? $endcolor"
echo "y"/"n"
read dropbox
echo
if [[ $dropbox == y ]]; then
	echo -e "$blue Installing Dropbox $endcolor"
	(
		apt-get install libappindicator1 nautilus-dropbox -y && cd ~ && rm -R ~/.dropbox-dist ; wget https://d1ilhw0800yew8.cloudfront.net/client/dropbox-lnx.x86_64-2.11.0.tar.gz && tar -xzvf dropbox-lnx.x86_64-2.11.0.tar.gz && cd .dropbox-dist/ && sh -c 'echo "alias dropbox=~/.dropbox-dist/dropboxd" >> ~/.bashrc'
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
fi

echo -e "$yellow Do you want to install VirtualBox 5 on your system? $endcolor"
echo "y"/"n"
read vbox
echo
if [[ $vbox == y ]]; then
	echo -e "$blue Installing VirtualBox 5 & DKMS $endcolor"
	(
		sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian vivid contrib" > /etc/apt/sources.list.d/virtualbox.list' && wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | apt-key add - && apt-get update && apt-get install virtualbox-5.0 dkms -y
	) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

	echo -e "$purple Downloading VirtualBox Extension on your HOME directory for manual installation... $endcolor";
	cd ~
	wget -q http://download.virtualbox.org/virtualbox/5.0.0/Oracle_VM_VirtualBox_Extension_Pack-5.0.0-101573.vbox-extpack
	echo -e "$purple Done, install extension when exit Ubuntu-Instaleitor $endcolor";
fi

# echo "Installing popcorntime, agree terms require interaction"
# (
# apt-get install popcorn-time -y
# ) # &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo "Updating System"
(
	apt-get update
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output

echo "Cleaning up"
(
	apt-get -y autoremove
	apt-get -y autoclean
	apt-get -y clean
) &> /dev/null && echo -e "$green OK $endcolor" || echo -e "$red FAILED $endcolor"; # Hide all output
echo

killall nautilus
tlp start

trap "rm -rf $tmp" EXIT # Delete tmp files on exit
echo
echo -e "$red THE PC WILL REBOOT IN 1 MINUTE, PLEASE WAIT... $endcolor";
echo "Enjoy, the installation has finished."
shutdown -r +1
