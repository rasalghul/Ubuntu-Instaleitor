Ubuntu-Instaleitor
====================================
Hello!
------
This script was done to make it easier Ubuntu after installation, the script installs the best software available in the GNU community.

Perhaps I forget add some software, if so let me know.

List of great Software that Ubuntu-Instaleitor install:

### Configuration
+ ubuntu-tweak
+ unity-tweak-tool
+ dconf-tools
+ compiz-plugins
+ compiz-plugins-extra
+ compizconfig-settings-manager

### Media
+ VLC
+ recordmydesktop
+ qBittorrent
+ clementine
+ spotify
+ K3B

### Utilities
+ indicator-netspeed applet
+ indicator-sysmonitor applet
+ redshift
+ speedtest-cli (command line speedtest)
+ nautilus-open-terminal
+ synapse
+ gparted
+ lm-sensors
+ hdd-temp
+ prelink
+ preload
+ htop
+ terminator
+ shutter
+ gsmartcontrol
+ tlp (for laptop battery)
+ boot-repair
+ encrypted-dvd
+ ubuntu-restricted-extras
+ curl

### Imaging
+ gimp
+ inkscape

### Development
+ Scudcloud (Unofficial Slack client for Linux)
+ sublime-text-3
+ atom
+ brackets
+ RVM
+ git

### Extras
+ pepperflashplugin-nonfree
+ dia (diagrams)
+ wireshark
+ hexchat
+ cheese
+ themes
+ filezilla
+ dropbox
+ virtualbox-5
+ dkms (virtualbox dependencies)

### Tweaks

+ disable Lens from Dash
+ disable apport crash dialogs
+ update & upgrade system
+ clean system


Install instructions:
------

+ Download the project .zip
+ Unzip on your desktop
+ Open terminal (ctrl+alt+t)
+ type: cd Desktop
+ give permission to Instaleitor, type: chmod +x Ubuntu-Instaleitor.sh
+ now, execute the script. type: sudo ./Ubuntu-Instaleitor.sh
+ Follow the instructions


May errors occur, we are in fase Beta.

Please feel free to contact me, give me your feedback.

Ubuntu-Instaleitor it is inspired by [Ninite.com](https://ninite.com/) and [Oduso.com](https://oduso.com/).

Twitter: @xxRasAlGhulxx